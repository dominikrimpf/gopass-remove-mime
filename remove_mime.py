import gnupg
import os
import re
from pathlib import Path
import argparse


def fix_store(store_path, recipients):
    store = os.path.expanduser(store_path)
    pathlist = Path(store).glob('**/*.gpg')
    for path in pathlist:
        with open(path, 'rb') as infile:
            decrypted = gpg.decrypt_file(infile)

        data = decrypted.data.decode('utf-8')
        if not regex.match(data):
            continue

        print(f'Fixing {str(path).removeprefix(store)}')
        fixed_data = regex.sub('', data)
        reencryption = gpg.encrypt(fixed_data, recipients, armor=False, output=path, always_trust=True)
        if reencryption.ok:
            print('Success')
        else:
            print('Failed')
            print(reencryption.status)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Fix Gopass password stores. (Remove MIME Types from files)')
    parser.add_argument('store', help='Password Store Path', type=str)
    parser.add_argument('recipients', help='Key Fingerprints of recipients', nargs='+', type=str)

    args = parser.parse_args()
    regex = re.compile(r'^GOPASS-SECRET-1\.0\n')
    gpg = gnupg.GPG(gnupghome=os.path.expanduser('~'), keyring=os.path.expanduser('~/.gnupg/pubring.kbx'))
    gpg.encoding = 'utf-8'

    fix_store(args.store, args.recipients)
